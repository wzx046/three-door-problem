#include<stdio.h>
#include<random>

#define times 10000000  /*测试次数*/

int prize[3] = { 0 };
int MyChoose;/*第一次选择*/
int noCarDoor;/*第一次没车的门*/
int Final_Choose;/*改变后的选择*/

void LoadCar()/*选定一扇门后有车*/
{
	for (int i = 0; i < 3; i++)/*清零*/
	{
		prize[i] = 0;
	}
	int car;
	car = rand() % 3;
	prize[car] = 1;
}

void choose()/*观众选择一扇门*/
{
	MyChoose = rand() % 3;
}

void First_openDoor()/*打开没车的门*/
{
	for (int i = 0; i < 3; i++)
	{
		if (i == MyChoose)continue;
		if (prize[i] == 0)
		{
			noCarDoor = i;
			break;
		}
	}
}

void ChangeDoor()/*改变选择*/
{
	Final_Choose = 3 - MyChoose - noCarDoor;
}

int main()
{
	float probability_c, probability_n;/*换门获胜概率与不换获胜概率*/
	int changenum = 0;/*换门获胜次数*/
	int nochangenum = 0;/*不换门获胜次数*/
	for (int j = 0; j < times; j++)
	{
		LoadCar();
		choose();
		First_openDoor();
		ChangeDoor();
		if (prize[Final_Choose] == 1)
			changenum++;
	}
	for (int j = 0; j < times; j++)
	{
		LoadCar();
		choose();
		First_openDoor();
		ChangeDoor();
		if (prize[MyChoose] == 1)
			nochangenum++;
	}
	probability_c = (float)changenum / times;
	probability_n = (float)nochangenum / times;
	printf("测试次数：%d\n", times);
	printf("换门获胜次数：%d      获胜概率：%f\n", changenum, probability_c);
	printf("不换门获胜次数：%d    获胜概率：%f", nochangenum, probability_n);
	return 0;
}